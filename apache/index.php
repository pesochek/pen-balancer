<h1>Hello World!</h1>

This is node <strong style='font-size: bigger;'>#<?= getenv('NODE_NUMBER') ?></strong>... Happy to serve you.

<blockquote style='background: #f0f0f0; padding: 10px; font-family: monospace;'>
    <p>current node IP = $_SERVER['SERVER_ADDR'] = <?= $_SERVER['SERVER_ADDR'] ?></p>
    <p>pen IP address = $_SERVER['REMOTE_ADDR'] = <?= $_SERVER['REMOTE_ADDR'] ?></p>
    <p>forwarded IP address (client IP) = $_SERVER['HTTP_X_FORWARDED_FOR'] = <?= $_SERVER['HTTP_X_FORWARDED_FOR'] ?></p>
</blockquote>

<hr/>

<?php phpinfo();