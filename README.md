## What's this

Tiny Docker configuration to check if [pen balancer](https://github.com/UlricE/pen) is able to downstream real client IP to the Apache nodes. It is expected to appear in `X_FORWARDED_FOR` HTTP request header (as per [this manual article](https://github.com/UlricE/pen/wiki/Transparent-Reverse-Proxy)). 

### Starting

* Clone repo
* Make sure Docker daemon is running
* Run with five Apache nodes:
```sh
sh ./start.sh 5
```
* Open [localhost](http://127.0.0.1/) in different incognito windows and monitor the result.

### Stopping

```sh
sh ./stop.sh
```