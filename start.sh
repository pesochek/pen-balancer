#!/bin/bash

NUMBER_OF_SERVERS=3

if [ ! -z "${1}" ]; then
    NUMBER_OF_SERVERS="${1}"
fi

./stop.sh

docker network inspect balancer-network 2> /dev/null

if [ "0" != "$?" ]; then
    docker network create balancer-network
fi

docker build \
    --force-rm \
    --tag balancer-pen \
    pen/

docker build \
    --force-rm \
    --tag balancer-apache \
    apache/

for ((i=1; i<=NUMBER_OF_SERVERS; i++)) do

    echo "Starting Apache #${i} on port 8${i}..."

    docker run \
        --detach \
        --env NODE_NUMBER=${i} \
        --name balancer-apache-${i} \
        --network=balancer-network \
        --publish 8${i}:80 \
        --rm \
        balancer-apache

done

echo "Starting pen balancer on port 80"

docker run \
    --env NUMBER_OF_SERVERS=${NUMBER_OF_SERVERS} \
    --name balancer-pen \
    --network=balancer-network \
    --publish 80:80 \
    --rm \
    balancer-pen