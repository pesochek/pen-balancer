#!/bin/bash

CMD="pen 0.0.0.0:80 -r -f -d -d -H -l /var/log/pen.log "

for ((i=1; i<=NUMBER_OF_SERVERS; i++)) do
    CMD+=" balancer-apache-${i}:80 "
done

echo ${CMD}
eval ${CMD}